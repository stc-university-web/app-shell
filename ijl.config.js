const pkg = require("./package.json");

module.exports = {
    apiPath: "stubs/api",
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${pkg.version}/`
        }
    },
    config: {
        'main.api.base.url': '/api',
    },
    features: {},
    navigations: {}
};

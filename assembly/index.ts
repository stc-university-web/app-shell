// The entry file of your WebAssembly module.

export function getConfig(): string {
  return '{"apps":{"app-shell":{"version":"1.0.0","name":"app-shell"}},"navigations":{"main":"\/main","app-shell":"\/app-shell"},"config":{"baseUrl":"\/static","main.api.base.url":"\/api"}}';
}
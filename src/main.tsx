import React from 'react';
import ReactDom from 'react-dom';


export default () => <div>Я родился</div>;

export const mount = (Component) => {
    const App = Component
    ReactDom.render(
        <App/>,
        document.getElementById('app')
    );
}

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'))
}

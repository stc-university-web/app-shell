const fs = require("fs");
const path = require("path");

const router = require("express").Router();

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


router.use("/optimized.wasm", (req, res) => {
    console.log(path.resolve(__dirname, '../../build/optimized.wasm'))
    res.sendFile(
        path.resolve(__dirname, '../../build/optimized.wasm')
    )
});


module.exports = router;